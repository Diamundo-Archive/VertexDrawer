
import java.awt.Rectangle;
/** http://docs.oracle.com/javase/7/docs/api/java/awt/Rectangle.html */

@SuppressWarnings("serial")
public class GraphVertex extends Rectangle {
	private String name;
    private Rectangle rectangle;
    
    public GraphVertex(int x, int y, int width, int height, String name){
    	this.rectangle = new Rectangle(x, y, width, height);
    	this.name = name;
    }
    
    public GraphVertex(String name) {
        this.name = name;
        this.rectangle = new Rectangle((int) (Math.random()*500 + 50), (int) (Math.random()*400 + 50), 100, 50); /* Standard size: creates rectangle at a somewhat random position of size (100, 50) */
    }
        
    public GraphVertex(){
        this.name = "New vertex";
        this.rectangle = new Rectangle((int) (Math.random()*500 + 50), (int) (Math.random()*400 + 50), 100, 50); /* Standard size: creates rectangle at a somewhat random position of size (100, 50) */
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Rectangle getRectangle(){
    	return rectangle;
    }
    
    public void setRectangle(Rectangle rectangle) {
    	this.rectangle = rectangle;
    }
    
    public void setRectangle(int x, int y, int width, int height) {
    	this.rectangle = new Rectangle(x, y, width, height);
    }
    
    public String toString(){
    	return ((int)this.rectangle.getX() + " " + (int)this.rectangle.getY() + " " + (int)this.rectangle.getWidth() + " " + (int)this.rectangle.getHeight() + " " + this.getName());
    }
    
}
