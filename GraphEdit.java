/** @author Kevin Nauta & Herman Groenbroek */

public class GraphEdit {
	public static void main(String[] arg) {
		
		GraphModel model = new GraphModel();
		@SuppressWarnings("unused")
		GraphFrame graphFrame = new GraphFrame(); 
		
		if(arg.length == 1) {
			model.load(arg[0]);
		}
	}
}
